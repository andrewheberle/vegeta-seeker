package main

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"math"
	"net/http"
	"os"
	"strings"
	"time"

	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	vegeta "github.com/tsenart/vegeta/v12/lib"
)

type AttackConfig struct {
	Target    string
	Metric    string
	RPS       int `mapstructure:"start"`
	Step      float64
	Success   float64
	Limit     time.Duration
	Duration  time.Duration
	Rest      time.Duration
	Max       int
	TargetRPS int `mapstructure:"rps"`
	Summary   bool
}

type Results struct {
	Start   time.Time `json:"start"`
	Finish  time.Time `json:"finish"`
	RPS     int       `json:"rps"`
	Latency Latency   `json:"latency"`
	Results []Result  `json:"results"`
	Runs    int       `json:"runs"`
}

type Latency struct {
	Min  time.Duration `json:"min"`
	Max  time.Duration `json:"max"`
	Mean time.Duration `json:"mean"`
	P50  time.Duration `json:"p50"`
	P90  time.Duration `json:"p90"`
	P95  time.Duration `json:"p95"`
	P99  time.Duration `json:"p99"`
}

type Result struct {
	RPS     int     `json:"rps"`
	Latency Latency `json:"latency"`
	Success float64 `json:"success"`
}

// newtargeter returns a vegeta.Targeter based on either a provided filename or standard input
func newtargeter(target string, body []byte, hdr http.Header) (vegeta.Targeter, error) {
	var buf *bytes.Buffer

	// if no target was specified, attempt to read from stdin
	if target == "" {
		stat, _ := os.Stdin.Stat()
		if (stat.Mode() & os.ModeCharDevice) == 0 {
			// read targets from stdin
			data, err := ioutil.ReadAll(os.Stdin)
			if err != nil {
				return nil, err
			}

			buf = bytes.NewBuffer(data)
		} else {
			return nil, fmt.Errorf("no target provided")
		}
	} else {
		// read targets from file
		data, err := ioutil.ReadFile(target)
		if err != nil {
			return nil, err
		}

		buf = bytes.NewBuffer(data)
	}

	// build targeter
	tr := vegeta.NewHTTPTargeter(buf, body, hdr)
	targets, err := vegeta.ReadAllTargets(tr)
	if err != nil {
		return nil, err
	}

	return vegeta.NewStaticTargeter(targets...), nil
}

// latency returns the requested latency value from the provided vegeta.LatencyMetrics
func latency(metric string, latencies vegeta.LatencyMetrics) time.Duration {
	switch strings.ToUpper(metric) {
	case "P99":
		return latencies.P99
	case "P95":
		return latencies.P95
	case "P90":
		return latencies.P90
	case "P50":
		return latencies.P50
	case "MIN":
		return latencies.Min
	case "MAX":
		return latencies.Max
	case "MEAN":
		return latencies.Mean
	}

	// this should not occur
	log.Fatal().Str("metric", metric).Send()

	return 0
}

// stepfreq increases the current frequency based on a maximum value and a provided step
func stepfreq(curr, max int, step float64) int {
	// calculate next value based on step
	next := int(math.Round(float64(curr) * step))

	// if next was less than max return next value as-is
	if next < max {
		return next

	}

	// otherwise return a value halfway to max
	return curr + ((max - curr) / 2)
}

// getlatency converts a set of vegeta.LatencyMetrics to our own Latency type
func getlatency(l vegeta.LatencyMetrics) Latency {
	return Latency{
		Min:  l.Min / LatencyDurationUnit,
		Max:  l.Max / LatencyDurationUnit,
		Mean: l.Mean / LatencyDurationUnit,
		P50:  l.P50 / LatencyDurationUnit,
		P90:  l.P90 / LatencyDurationUnit,
		P95:  l.P95 / LatencyDurationUnit,
		P99:  l.P99 / LatencyDurationUnit,
	}
}

// Attack is the main function that executes the benchmark
func Attack(config AttackConfig) (Results, error) {
	if config.Summary {
		zerolog.SetGlobalLevel(zerolog.WarnLevel)
	}
	// initial results
	results := Results{
		Start:   time.Now(),
		Results: make([]Result, 0),
	}

	// list of targets
	targeter, err := newtargeter(config.Target, nil, nil)
	if err != nil {
		log.Fatal().Err(err).Send()
	}

	// create attacker
	attacker := vegeta.NewAttacker()

	rps := config.RPS
	okrps := rps
	failrps := math.MaxInt
	n := 0
	for {
		var metrics vegeta.Metrics
		var status string
		logger := log.With().Int("rps", rps).Logger()

		// status at start
		logger.Info().Msg("starting attack")

		// start attack
		for res := range attacker.Attack(targeter, vegeta.ConstantPacer{Freq: rps, Per: time.Second}, config.Duration, fmt.Sprintf("Attack at %d rps", rps)) {
			metrics.Add(res)
		}
		metrics.Close()

		// add to results
		results.Results = append(results.Results, Result{
			RPS:     rps,
			Latency: getlatency(metrics.Latencies),
			Success: metrics.Success,
		})
		results.Runs = n

		// add latency to our logger
		logger = logger.With().
			Dur("min", metrics.Latencies.Min).
			Dur("max", metrics.Latencies.Max).
			Dur("mean", metrics.Latencies.Mean).
			Dur("P50", metrics.Latencies.P50).
			Dur("P90", metrics.Latencies.P90).
			Dur("P95", metrics.Latencies.P95).
			Dur("P99", metrics.Latencies.P99).
			Float64("success", metrics.Success).
			Logger()

		// increment counter and check if we are done
		n++
		if n >= config.Max {
			logger.Info().Str("status", StatusMaximumRunsReached).Send()
			results.Latency = getlatency(metrics.Latencies)
			break
		}

		// check latency and success rate was within limits
		switch {
		case latency(config.Metric, metrics.Latencies) > config.Limit && metrics.Success < config.Success:
			status = StatusBothExceeded
		case latency(config.Metric, metrics.Latencies) > config.Limit:
			status = StatusLatencyLimitExceeded
		case metrics.Success < config.Success:
			status = StatusErrorRateExceeded
		default:
			status = StatusOK
		}
		if status != StatusOK {
			// track our max we failed at
			failrps = rps

			// set frequency back to previous good value
			rps = okrps
		} else {
			// if limit was OK then make a note of it
			okrps = rps
		}

		// increase frequency for next attack
		rps = stepfreq(rps, failrps, config.Step)

		// if step produced no increase then finish up here
		if rps <= okrps {
			logger.Info().Str("status", StatusRPSLimitReached).Msg("attack complete")
			results.Latency = getlatency(metrics.Latencies)
			break
		}

		// output results before next run
		logger.Info().Str("status", status).Msg("attack complete")

		// rest for a while
		time.Sleep(config.Rest)
	}

	// set final results
	results.RPS = rps
	results.Finish = time.Now()

	// check if a target rps was reached (default is 0 which unless something is very wrong should always be successful)
	if rps < config.TargetRPS {
		return results, fmt.Errorf("required rps not reached")
	}

	return results, nil
}
