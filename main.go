package main

import (
	"encoding/json"
	"fmt"
	"os"
	"strconv"
	"strings"
	"time"

	"github.com/rs/zerolog/log"
	"github.com/spf13/pflag"
	"github.com/spf13/viper"
)

// metricvar is a custom type to handle parsing and validation of --metric command line flag by satisfyling the pflag.Value interface
type metricvar string

func (m *metricvar) Set(s string) error {
	s = strings.ToUpper(s)
	if s != "P99" && s != "P95" && s != "P90" && s != "P50" && s != "MIN" && s != "MAX" && s != "MEAN" {
		return fmt.Errorf("must be one of MIN, MAX, MEAN, P50, P90, P95 or P99")
	}

	*m = metricvar(s)

	return nil
}

func (m *metricvar) String() string {
	return string(*m)
}

func (m *metricvar) Type() string {
	return "string"
}

// percentvar is a custom type to handle parsing and validation of --metric command line flag by satisfyling the pflag.Value interface
type percentvar float64

func (p *percentvar) Set(s string) error {
	f, err := strconv.ParseFloat(s, 64)
	if err != nil {
		return err
	}

	if f > 1 || f <= 0 {
		return fmt.Errorf("must be >= 0 and <= 1")
	}

	*p = percentvar(f)

	return nil
}

func (p *percentvar) String() string {
	return strconv.FormatFloat(float64(*p), 'g', -1, 64)
}

func (p *percentvar) Type() string {
	return "float64"
}

func main() {
	// default metric to test
	var metric = metricvar("P95")
	var success = percentvar(1.0)
	var config AttackConfig

	// command line arg handling
	pflag.Duration("limit", 1000*time.Millisecond, "Latency ceiling for tests")
	pflag.Duration("duration", 30*time.Second, "Duration of each test")
	pflag.Var(&success, "success", "Required success rate of requests")
	pflag.Var(&metric, "metric", "Latency metric to compare (mean, min, max, P50, P90, P95 or P99)")
	pflag.String("target", "", "File containing a list of targets")
	pflag.Int("start", 8, "Starting rate per second")
	pflag.Int("rps", 0, "Target rate per second (optional)")
	pflag.Float64("step", 2, "Rate per second step factor between tests")
	pflag.Int("max", 100, "Maximum runs")
	pflag.Duration("rest", 5*time.Second, "Rest time between attacks")
	pflag.Bool("summary", false, "Output summary at conclusion of test (no progress messages)")
	pflag.Parse()

	// bind command line args to viper
	if err := viper.BindPFlags(pflag.CommandLine); err != nil {
		log.Fatal().Err(err).Send()
	}

	// support use of env vars so command line options can be specified via VS_<FLAG>=whatever
	viper.SetEnvPrefix("vs")
	viper.AutomaticEnv()

	// unmarshal command line into out config struct
	if err := viper.Unmarshal(&config); err != nil {
		log.Fatal().Err(err).Msg("config error")
	}

	// start attack
	results, err := Attack(config)

	// output summary
	if viper.GetBool("summary") {
		encoder := json.NewEncoder(os.Stdout)
		encoder.SetIndent("", "  ")
		if err := encoder.Encode(results); err != nil {
			log.Error().Err(err).Send()
		}
	}

	// finish with a non-zero exit code if target RPS was not reached
	if err != nil {
		os.Exit(1)
	}
}
