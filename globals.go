package main

import "time"

var (
	LatencyDurationUnit = time.Millisecond
)

// some constants for later
const (
	StatusOK                   = "ok"
	StatusErrorRateExceeded    = "error rate exceeded"
	StatusLatencyLimitExceeded = "latency limit exceeded"
	StatusBothExceeded         = "latency limit and error rate exceeded"
	StatusMaximumRunsReached   = "maximum runs reached"
	StatusRPSLimitReached      = "limit reached"
)
