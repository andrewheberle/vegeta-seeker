# vegeta-seeker

This is a very simple benchmark tool to test a web service and until it reaches a configured latency threshold.

The underlying library is [https://github.com/tsenart/vegeta](https://github.com/tsenart/vegeta) so this is written in Go.

## Installation

Assuming you have a functional Go installation, the following command will compile vegeta-seeker:

```sh
go install gitlab.com/andrewheberle/vegeta-seeker@latest
```

## Theory of Operation

This tool will perform HTTP requests at a starting value (default is 8-requests per second) and subsequently double the RPS each time and continue this process until the service reponse time (latency) exceeds a configured value (default is 1000ms).

At this point the RPS will be reduced back to half way between the last successful run and the current failed value.

This process repeats until the RPS cannot be increased any further which gives a basic theoretical maximum RPS for the service.

## Usage

The simplest possible usage is:

```sh
echo "GET http://target" | ./vegeta-seeker
```

In addition a text file can be provided as per the format used for the [https://github.com/tsenart/vegeta#http-format](Vegeta http format) as follows:

```sh
./vegeta-seeker --target /path/to/targets.txt
```

An example targets file is as follows:

```txt
GET http://target1
GET http://target2:8080
GET http://target3/path/to/api
```

## Command Line Options

```sh
vegeta-seeker --help
Usage of vegeta-seeker:
      --duration duration   Duration of each test (default 30s)
      --limit duration      Latency ceiling for tests (default 1s)
      --max int             Maximum runs (default 100)
      --metric string       Latency metric (mean, min, max, P50, P90, P95 or P99) (default "P95")
      --rest duration       Rest time between attacks (default 5s)
      --rps int             Target rate per second
      --start int           Starting rate per second (default 8)
      --step float          Rate per second step factor between tests (default 2)
      --target string       File containing a list of targets
```

## Environment Variables

Any option that can be provided on the command line can be set using environment variables by prefixing `VS_` to the name of the option.

For example:

```sh
VS_METRIC="P99" VS_TARGET="/path/to/targets.txt" ./vegeta-seeker
```

## Limitations

1. This tool does not implement all available options of the underlying Vegeta library and does not attempt to duplicate the functionality of the Vegeta command line tool.
2. The testing methodoloy is somewhat naive as multiple runs at each RPS are not executed to account for variations
3. Detailed results are not recorded so the analysis options using Vegeta natively are not possible
